package com.example.springredistests;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository
    extends CrudRepository<Student, String> {

  Optional<Student> findStudentByName(String name);

}
