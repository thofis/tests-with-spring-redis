package com.example.springredistests;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address {

  private String street;
  private String postalCode;
  private String city;

}
