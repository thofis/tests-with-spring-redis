package com.example.springredistests;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.example.springredistests.Student.Gender.FEMALE;
import static com.example.springredistests.Student.Gender.MALE;

@SpringBootApplication
public class
SpringRedisTestsApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringRedisTestsApplication.class, args);
  }

  @Bean
  RedisTemplate<String, Student> redisTemplate(RedisConnectionFactory rcf) {

    RedisTemplate<String, Student> template = new RedisTemplate<>();
    template.setConnectionFactory(rcf);
    // used by @JsonBasedStudentRepository
    template.setKeySerializer(new StringRedisSerializer());
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Student.class));

    return template;
  }

  @Bean
  RedisTemplate<String, Map<byte[], byte[]>> redisTemplateForHashes(RedisConnectionFactory rcf) {
    RedisTemplate<String, Map<byte[], byte[]>> template = new RedisTemplate<>();
    template.setConnectionFactory(rcf);
    // used by @ObjectMapperStudentRepository
    template.setKeySerializer(new StringRedisSerializer());
    template.setEnableDefaultSerializer(false);
    template.afterPropertiesSet();
    return template;
  }

}

/**
 * @implNote https://www.baeldung.com/spring-data-redis-tutorial
 */
@Component
@Slf4j
class RedisTester implements ApplicationRunner {

  // auto generated repo from spring data redis
  private final StudentRepository studentRepository;

  // manually implemented using jackson to serialize as json
  private final JsonBasedStudentRepository jsonBasedStudentRepositoryRepository;

  // manually implemented using ObjectHashMapper to serialize as strings in a redis hash
  private final ObjectMapperStudentRepository objectMapperStudentRepository;

  RedisTester(StudentRepository studentRepository, JsonBasedStudentRepository jsonBasedStudentRepositoryRepository, ObjectMapperStudentRepository objectMapperStudentRepository) {
    this.studentRepository = studentRepository;
    this.jsonBasedStudentRepositoryRepository = jsonBasedStudentRepositoryRepository;
    this.objectMapperStudentRepository = objectMapperStudentRepository;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    String id1 = getUniqueId();
    Student student = Student.builder()
                             .id(id1)
                             .name("John Doe")
                             .gender(MALE)
                             .grades(List.of(2, 2))
                             .address(Address.builder()
                                             .street("street 2")
                                             .postalCode("23456")
                                             .city("another city")
                                             .build())
                             .build();
    jsonBasedStudentRepositoryRepository.save(student);

    String id2 = getUniqueId();
    student = Student.builder()
                     .id(id2)
                     .name("Jane Doe")
                     .gender(FEMALE)
                     .grades(List.of(1, 2, 3))
                     .address(Address.builder()
                                     .street("street 1")
                                     .postalCode("12345")
                                     .city("city")
                                     .build())
                     .build();
    jsonBasedStudentRepositoryRepository.save(student);

    jsonBasedStudentRepositoryRepository.findById(id2)
                                        .ifPresent(student1 -> log.info(student1.toString()));

    // expected to be true
    log.info("" + jsonBasedStudentRepositoryRepository.existsById(id1));
    // expected to be false
    log.info("" + jsonBasedStudentRepositoryRepository.existsById(getUniqueId()));

    String id3 = getUniqueId();
    student = Student.builder()
                     .id(id3)
                     .name("Jack Hash")
                     .gender(MALE)
                     .grades(List.of(2, 2, 2))
                     .address(Address.builder()
                                     .street("street 3")
                                     .postalCode("33333")
                                     .city("city3")
                                     .build())
                     .build();

    objectMapperStudentRepository.save(student);
    objectMapperStudentRepository.findById(id3)
                                 .ifPresent(student3 -> log.info(student3.toString()));


    String id4 = getUniqueId();
    student = Student.builder()
                     .id(id4)
                     .name("Joe Proxy")
                     .gender(MALE)
                     .grades(List.of(3, 2, 1))
                     .address(Address.builder()
                                     .street("street 4")
                                     .postalCode("44444")
                                     .city("city4")
                                     .build())
                     .build();

    studentRepository.save(student);
    studentRepository.findById(id4)
                     .ifPresent(student4 -> log.info(student4.toString()));
    studentRepository.findStudentByName(student.getName())
                     .ifPresent(student5 -> log.info(student5.toString()));
    studentRepository.existsById(id4);

//    String id5 = getUniqueId();
//    student = Student.builder()
//                     .id(id5)
//                     .name("Joe Proxy2")
//                     .gender(MALE)
//                     .grades(List.of(3, 2, 1))
//                     .address(Address.builder()
//                                     .street("street 4")
//                                     .postalCode("44444")
//                                     .city("city4")
//                                     .build())
//                     .build();
//
//    studentRepository.findAll().forEach(st->log.info(st.toString()));


  }

  private String getUniqueId() {
    return UUID.randomUUID()
               .toString();
  }
}


