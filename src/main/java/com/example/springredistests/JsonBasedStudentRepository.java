package com.example.springredistests;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@Repository
public class JsonBasedStudentRepository implements CrudRepository<Student, String> {

  private final RedisTemplate<String, Student> redisTemplate;
  private final String key;

  private long ttl;

  public JsonBasedStudentRepository(RedisTemplate<String, Student> redisTemplate) {
    this.redisTemplate = redisTemplate;
    key = Student.getHashNameFromAnnotation();
    ttl = Student.getTimeToLiveFromAnnotation();
  }

  @Override
  public <S extends Student> S save(S entity) {
    redisTemplate.opsForValue()
                 .set(getHashKey(entity.getId()), entity);
    redisTemplate.expire(getHashKey(entity.getId()), ttl, TimeUnit.SECONDS);
    return entity;
  }

  @Override
  public <S extends Student> Iterable<S> saveAll(Iterable<S> entities) {
    return null;
  }

  @Override
  public Optional<Student> findById(String id) {
    var student = redisTemplate.opsForValue()
                               .get(getHashKey(id));
    return Optional.ofNullable(student);
  }

  @Override
  public boolean existsById(String id) {
    return Boolean.TRUE.equals(redisTemplate.hasKey(getHashKey(id)));
  }

  @Override
  public Iterable<Student> findAll() {
    return null;
  }

  @Override
  public Iterable<Student> findAllById(Iterable<String> strings) {
    return null;
  }

  @Override
  public long count() {
    return 0;
  }

  @Override
  public void deleteById(String s) {

  }

  @Override
  public void delete(Student entity) {

  }

  @Override
  public void deleteAll(Iterable<? extends Student> entities) {

  }

  @Override
  public void deleteAll() {

  }

  private String getHashKey(String id) {
    return String.format("%s:%s", key, id);
  }

}
