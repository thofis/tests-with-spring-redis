package com.example.springredistests;

import lombok.*;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.util.List;
import java.util.Objects;

@RedisHash(value = "test:Student", timeToLive = 300)
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Student {


  public enum Gender {
    MALE, FEMALE
  }

  private String id;
  @Indexed private String name;
  private Gender gender;
  private List<Integer> grades;
  private Address address;
  private String empty;

  static String getHashNameFromAnnotation() {
    RedisHash annotation = AnnotationUtils.findAnnotation(Student.class, RedisHash.class);
    Objects.requireNonNull(annotation);
    return annotation.value();
  }

  static long getTimeToLiveFromAnnotation() {
    RedisHash annotation = AnnotationUtils.findAnnotation(Student.class, RedisHash.class);
    Objects.requireNonNull(annotation);
    return annotation.timeToLive();
  }




}
