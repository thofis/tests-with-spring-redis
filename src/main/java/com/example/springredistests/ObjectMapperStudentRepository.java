package com.example.springredistests;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.hash.HashMapper;
import org.springframework.data.redis.hash.ObjectHashMapper;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

@Repository
public class ObjectMapperStudentRepository implements CrudRepository<Student, String> {

  private final HashOperations<String, byte[], byte[]> hashOperations;

  private final RedisTemplate<String, Map<byte[], byte[]>> redisTemplate;
  private final HashMapper<Object, byte[], byte[]> mapper;

  private final String key;

  private long ttl;

  public ObjectMapperStudentRepository(RedisTemplate<String, Map<byte[], byte[]>> redisTemplate) {
    this.redisTemplate = redisTemplate;
    this.hashOperations = redisTemplate.opsForHash();
    this.mapper = new ObjectHashMapper();
    this.key = Student.getHashNameFromAnnotation();
    this.ttl = Student.getTimeToLiveFromAnnotation();
  }


  @Override
  public <S extends Student> S save(S entity) {
    Map<byte[], byte[]> mappedHash = mapper.toHash(entity);
    String hashKey = getHashKey(entity.getId());
    hashOperations.putAll(hashKey, mappedHash);
    redisTemplate.expire(hashKey, ttl, SECONDS);
    return entity;
  }

  @Override
  public <S extends Student> Iterable<S> saveAll(Iterable<S> entities) {
    return null;
  }

  @Override
  public Optional<Student> findById(String id) {
    Map<byte[], byte[]> loadedHash = hashOperations.entries(getHashKey(id));
    Student student = (Student) mapper.fromHash(loadedHash);
    return Optional.of(student);
  }

  @Override
  public boolean existsById(String s) {
    return false;
  }

  @Override
  public Iterable<Student> findAll() {
    return null;
  }

  @Override
  public Iterable<Student> findAllById(Iterable<String> strings) {
    return null;
  }

  @Override
  public long count() {
    return 0;
  }

  @Override
  public void deleteById(String s) {

  }

  @Override
  public void delete(Student entity) {

  }

  @Override
  public void deleteAll(Iterable<? extends Student> entities) {

  }

  @Override
  public void deleteAll() {

  }

  private String getHashKey(String id) {
    return String.format("%s:%s", key, id);
  }
}
